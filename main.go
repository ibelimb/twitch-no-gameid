package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/nicklaw5/helix"
)

// Configuration struct
type Configuration struct {
	ClientID     string `json:"ClientID"`
	ClientSecret string `json:"ClientSecret"`
}

// ViewData struct
type ViewData struct {
	Streams []helix.Stream
	Start   int
}

// Global variables
var globalStreams []helix.Stream
var globalRunning = false

// View handler
func viewHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, _ := template.New("view.html").Funcs(template.FuncMap{
		"processThumbnailURL": func(url string) string {
			url = strings.Replace(url, "{width}", "256", 1)
			url = strings.Replace(url, "{height}", "256", 1)
			return url
		},
		"mod": func(i int, c int) bool {
			if (i+1)%c == 0 {
				return true
			}
			return false
		},
	}).ParseFiles("view.html")

	var start, end int
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}

	if start+50 > len(globalStreams) {
		end = len(globalStreams)
	} else {
		end = start + 50
	}

	viewdata := ViewData{
		Streams: globalStreams[start:end],
		Start:   end,
	}

	tmpl.Execute(w, viewdata)
}

func main() {
	file, _ := os.Open("config.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err := decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Println(configuration.ClientID) // output: [UserA, UserB]

	client, err := helix.NewClient(&helix.Options{
		ClientID:      configuration.ClientID,
		ClientSecret:  configuration.ClientSecret,
		RateLimitFunc: rateLimitCallback,
	})
	if err != nil {
		// handle error
	}

	go getAllStreams(client)

	http.HandleFunc("/", viewHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func getAllStreams(client *helix.Client) {
	if globalRunning {
		return
	}
	globalRunning = true
	var cursor = ""

	for {
		resp, _ := client.GetStreams(&helix.StreamsParams{
			GameIDs: []string{"0"},
			First:   100,
			After:   cursor,
		})

		cursor = resp.Data.Pagination.Cursor
		globalStreams = append(globalStreams, resp.Data.Streams...)

		fmt.Printf("Cursor: %s\n", resp.Data.Pagination.Cursor)

		if resp.Data.Pagination.Cursor == "" {
			break
		}

		time.Sleep(2 * time.Second) // Try not to slam the server
	}
	globalRunning = false
}

func rateLimitCallback(lastResponse *helix.Response) error {
	if lastResponse.GetRateLimitRemaining() > 0 {
		return nil
	}

	var reset64 int64
	reset64 = int64(lastResponse.GetRateLimitReset())

	currentTime := time.Now().Unix()

	if currentTime < reset64 {
		timeDiff := time.Duration(reset64 - currentTime)
		if timeDiff > 0 {
			fmt.Printf("Waiting on rate limit to pass before sending next request (%d seconds)\n", timeDiff)
			time.Sleep(timeDiff * time.Second)
		}
	}

	return nil
}
